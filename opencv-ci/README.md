
Docker file:

* base image: ubuntu 16.04
* python version 3.6.1 compiled from source
* python 2.7 from ubuntu repositories
* and opencv 3.2 compiled from source with python2.7 and python3 support

The purpose of the image is to be used as bitbucket piplines image.

