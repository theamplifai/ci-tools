#!/usr/bin/env bash


function regenerate_pypi_rc_file() {
    FILE_NAME=~/.pypirc
    if [ -e $FILE_NAME ]
    then
        rm $FILE_NAME
    fi
    echo [distutils] >> $FILE_NAME
    echo index-servers=pypicloud >> $FILE_NAME
    echo [pypicloud] >> $FILE_NAME
    echo repository=$PYPICLOUD_URL >> $FILE_NAME
    echo username=$PYPICLOUD_USER >> $FILE_NAME
    echo password=$PYPICLOUD_PASS >> $FILE_NAME
}


function update_package_version_and_upload() {
    bumpversion patch --verbose
    python setup.py sdist upload -r pypicloud

}

function update_git() {
    echo Setting git identity
    git config --local user.name "pipelines"
    git config --local user.email "admin@amplifai.co"
    git commit -am "Version Bump [skip ci]"
    NEW_VER=v$(python setup.py --version)
    git tag -a $NEW_VER -m "Version $NEW_VER"
    git push origin master --tags

}

echo Deploying Python package
regenerate_pypi_rc_file
echo Uploading to PyPI repository
update_package_version_and_upload
echo Updating Git Repo
update_git
