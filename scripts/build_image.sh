#!/usr/bin/env bash

FOLDER=${1%/}
VER=$2
IMAGE_NAME=$3

if [ -z $IMAGE_NAME ]
then
    IMAGE_NAME=$FOLDER
fi

if [ -z $VER ]
then
    echo Please supply tag version:
    echo Example:
    echo \$ ./build_image.sh light-weight-ci 0.0.1
fi

echo docker build -t theamplifai/$IMAGE_NAME:$VER -f $FOLDER/Dockerfile .
docker build -t theamplifai/$IMAGE_NAME:$VER --file=$FOLDER/Dockerfile .