#!/usr/bin/env bash


IMAGE_NAME=$1
VER=$2

if [ -z $VER ] || [ -z $IMAGE_NAME ]
then
    echo Please supply tag version and image name:
    echo Example:
    echo \$ ./run_container.sh light-weight-ci 0.0.1
fi

docker run -it --workdir="/localDebugRepo" --memory=4g --memory-swap=4g --entrypoint=/bin/bash theamplifai/$IMAGE_NAME:$VER
