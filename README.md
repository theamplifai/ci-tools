# CI Tools

This repository contains scripts for managing the Bitbucket Pipelines CI.
## Docker containers
There are 2 docker images used in continuous integrarion:
* [light-weight-ci](https://hub.docker.com/r/theamplifai/light-weight-ci/) - For repositories that don not depend on python-opencv.
* [opencv-ci](https://hub.docker.com/r/theamplifai/opencv-ci/) - For repositories that depend on opencv.

To build the containers run:
```
$ ./scripts/build_image.sh FOLDER VERSION [IMAGE_NAME] 
```
where:

- `FOLDER` is the folder containing the _Dockerfile_.
- `VERSION` is the tag that will be applied to the container.
- `IMAGE_NAME` the name of the container _theamplifai/<image_name>_. If not supplied, `FOLDER` will used.

## Scripts
Scripts for maintaing the build environment:
* _build_image.sh_ used to build docker images
* _run_container.sh_ used to run docker container locally. (Usually if someone wants to test-run a build process locally)
* _deploy_pypi_package.sh_ Executed from PipelinesCI after a successfull build to push a python package to amplifai pypi server.

## Files
* _pylintrc_ configuration file for pylint.